const app = require('express')();
const http = require('http').Server(app);

const io = require('socket.io')(http,{
    cors: {
        origin:  /.*/
      }
});

/*
app.options('*', require('cors')())
app.use(require('cors')({
    origin:allowedOrigins
}))
*/

app.get('/', (req, res) => {
  res.send('OK')
});

io.on('connection', (socket) => {
  console.log('a user connected');
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});